<?php
/* should return $paymentOk, $paymentAmountGross, $paymentAmountNet, $paymentRef, $paymentResponse vars */
$amount = $invoice->getTotalAmount();

$paymentOk = true;
$paymentAmountGross = $amount;
$paymentAmountNet = 0.90 * $paymentAmountGross;

$paymentRef = ntsLib::generateRand(8);
$paymentResponse = ntsLib::generateRand(24);
?>