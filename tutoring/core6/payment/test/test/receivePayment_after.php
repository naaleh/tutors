<?php
$paymentOkUrl = ntsLink::makeLink( 
	'customer/invoices/view',
	'', 
	array(
		'refno' => $invoiceRefNo,
		'display' => 'ok'
		)
	);
$paymentFailedUrl = ntsLink::makeLink( 
	'customer/invoices/view',
	'',
	array(
		'refno' => $invoiceRefNo,
		'display' => 'fail'
		) 
	);

$url = $paymentOk ? $paymentOkUrl : $paymentFailedUrl;
ntsView::redirect( $url );
?>