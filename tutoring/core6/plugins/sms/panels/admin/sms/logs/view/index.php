<?php
$e = $NTS_VIEW['e'];

$view = array();

$t = new ntsTime( $e['sent_at'] );
$cellView = $t->formatWeekdayShort() . ', ' . $t->formatDate() . ' ' . $t->formatTime();
$view[] = array(
	M('Sent At'),
	$cellView
	);

$t = new ntsTime( $e['sent_at'] );
$cellView = $t->formatWeekdayShort() . ', ' . $t->formatDate() . ' ' . $t->formatTime();
if( $e['success'] )
	$cellView = '<span class="label label-success">OK</span>';
else
	$cellView = '<span class="label label-danger">' . M('Failed') . '</span>';
$view[] = array(
	M('Status'),
	$cellView
	);

$view[] = array(
	M('To'),
	$e['to_number']
	);

$view[] = array(
	M('From'),
	$e['from_number']
	);

$view[] = array(
	M('Message'),
	nl2br($e['message'])
	);

$view[] = array(
	M('Response'),
	$e['response']
	);

?>

<dl>
<?php foreach( $view as $v ) : ?>
	<dt>
		<?php echo $v[0]; ?>
	</dt>
	<dd>
		<?php echo $v[1]; ?>
	</dd>
<?php endforeach; ?>
</dl>