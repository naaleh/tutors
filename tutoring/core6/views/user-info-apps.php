<div class="tab-content">
	<div id="my_app" class="tab-pane fade <? echo ($_REQUEST['pg'] == "my_app" ? 'in active' : ''); ?>">
		<div class="wrap">
			<a name="appointments"></a>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css">
    <link href="/./daterangepicker-master/jquery.comiseo.daterangepicker.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/2.3.1/moment.min.js"></script>
    <script src="/./daterangepicker-master/jquery.comiseo.daterangepicker.js"></script>
    <script>
        $(function() { $("#e1").daterangepicker({presetRanges: [{
         text: 'Today',
         dateStart: function() { return moment() },
         dateEnd: function() { return moment() }
     }, {
         text: 'Tomorrow',
         dateStart: function() { return moment().add('days', 1) },
         dateEnd: function() { return moment().add('days', 1) }
     }, {
         text: 'Next Week',
         dateStart: function() { return moment().add('weeks', 1).startOf('week') },
         dateEnd: function() { return moment().add('weeks', 1).endOf('week') }

     }, {
         text: 'This Month',
         dateStart: function() { return moment().startOf('month') },
         dateEnd: function() { return moment().endOf('month') }
     }], datepickerOptions: {
         minDate: null,
         maxDate: null 
        }
     }) });
    </script>
<form action="http://naalehtutors.com/tutoring/?nts-panel=customer/book" method="post">
    <input type="hidden" name = "pg" value="my_app">
    <input id="e1" name="e1">
    <input type="submit" value="Search">
    </form>

<?php

$current_panel = $_NTS['CURRENT_PANEL'];
if( substr($current_panel, 0, strlen('customer/appointments')) == 'customer/appointments' ){
	return;
}

unset($dates);
 //$dates = $_NTS['REQ']->getParam('e1');
$dates = $_POST['e1'];

$dates = json_decode($dates , true);





// get upcoming appointments
$current_user = ntsLib::getCurrentUser();
if( ! $current_user->getId() ){
	return;
}

$ntsdb =& dbWrapper::getInstance();
$customer_id = $current_user->getId();

global $NTS_VIEW;

if( ! isset($NTS_VIEW['t']) ){
	$t = new ntsTime();
	global $NTS_CURRENT_USER;
	$t->setTimezone( $NTS_CURRENT_USER->getTimezone() );
	$NTS_VIEW['t'] = $t;
}

$t = $NTS_VIEW['t'];
$t->setNow();
$t->setStartDay();
$startToday = $t->getTimestamp();


$date = new DateTime($dates['start']);

$from = $date->getTimestamp();



$date = new DateTime($dates['end'] . "23:59:59");

$to = $date->getTimestamp();


$where = array(
	'customer_id'	=> array( '=', $customer_id )
	);
$where['starts_at'] = array( '>=', $startToday );

 //$where['starts_at'] = array( '<=', $to);
 //$where['starts_at '] = array( '<=', $from );

$where['completed'] = array( '<>', HA_STATUS_CANCELLED );
$where['completed '] = array( '<>', HA_STATUS_NOSHOW );

$addon = 'ORDER BY starts_at ASC';

$count_upcoming_appointments = ntsObjectFactory::count( 'appointment', $where, $addon );
//ntsObjectFactory::find( 'appointment', $where, $addon );



//print_r($objects); exit;

$where['starts_at'] = array( '<', $startToday );
$count_past_appointments = ntsObjectFactory::count( 'appointment', $where, $addon );
// $count_past_appointments = 0;
$total_appointments = $count_upcoming_appointments + $count_past_appointments;

$where['starts_at'] = array( '<=', $to );
$where['starts_at '] = array( '>=', $from );
$count_filtered_appointments = ntsObjectFactory::count( 'appointment', $where, $addon );

?>


<?php if(isset($dates) && $count_filtered_appointments == 0) {
      echo("You have no appointments for the selected dates");
}?>
<?php if($count_filtered_appointments > 0) : ?>
	<div class="collapse-panel panel panel-group panel-default nts-ajax-parent" style="margin: 0.5em 0;">
		<div class="panel-heading">
			<h4 class="panel-title">
				<div class="pull-right">
					<span class="badge badge-default"><?php echo $count_filtered_appointments; ?></span>
				</div>
				<a href="<?php echo ntsLink::makeLink('customer/appointments/view', '', array('show' => 'daterange', 'from'  => $from, 'to'  => $to,  'display' => 'short')); ?>" data-toggle="collapse-next" class="display-block nts-ajax-loader">
					<?php 	echo M("Today's Appointments"); ?> 
					<span class="caret"></span>
				</a>
			</h4>
		</div>

		<div class="panel-collapse collapse">
			<div class="panel-body nts-ajax-container">
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if( $count_upcoming_appointments ) : ?>
	<div class="collapse-panel panel panel-group panel-default nts-ajax-parent" style="margin: 0.5em 0;">
		<div class="panel-heading">
			<h4 class="panel-title">
				<div class="pull-right">
					<span class="badge badge-default"><?php echo $count_upcoming_appointments; ?></span>
				</div>
				<a href="<?php echo ntsLink::makeLink('customer/appointments/view', '', array('show' => 'upcoming', 'display' => 'short')); ?>" data-toggle="collapse-next" class="display-block nts-ajax-loader">
					<?php echo M('My Upcoming Appointments'); ?> <span class="caret"></span>
				</a>
			</h4>
		</div>

		<div class="panel-collapse collapse">
			<div class="panel-body nts-ajax-container">
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if( $count_past_appointments ) : ?>
	<div class="collapse-panel panel panel-group panel-default nts-ajax-parent" style="margin: 0.5em 0;">
		<div class="panel-heading">
			<h4 class="panel-title">
				<div class="pull-right">
					<span class="badge badge-default"><?php echo $count_past_appointments; ?></span>
				</div>
				<a href="<?php echo ntsLink::makeLink('customer/appointments/view', '', array('show' => 'old', 'display' => 'short')); ?>" data-toggle="collapse-next" class="display-block nts-ajax-loader">
					<?php echo M('My Past Appointments'); ?> <span class="caret"></span>
				</a>
			</h4>
		</div>

		<div class="panel-collapse collapse">
			<div class="panel-body nts-ajax-container">
			</div>
		</div>
	</div>
<?php endif; ?>
</div></div>