<?php
$id = $this->getValue('id');
/* form params - used later for validation */
$this->setParams(
	array(
		'myId'	=> $id,
		)
	);

$object = $this->getValue('object'); ?>






<? 	echo ntsForm::wrapInput(
		M('Tutor Name'),
		$this->buildInput (
		/* type */
			'text',
		/* attributes */
			array(
				'id'	=> 'tutor_name',
                        'default' => $object->getProp('first_name') . ' ' . $object->getProp('last_name')
                        
				)
                
			
			)			
		);
    
	?>


<? 	echo ntsForm::wrapInput(
		M('Student Name'),
		$this->buildInput (
		/* type */
			'text',
		/* attributes */
			array(
				'id'	=> 'student_name', 
                        
				)
                
			
			)			
		);
    
	?>


<? 	echo ntsForm::wrapInput(
		M('Date of Meeting'),
		$this->buildInput (
		/* type */
			'text',
		/* attributes */
			array(
				'id'	=> 'date', 
                        
				)
                
			
			)			
		);
    
	?>



<? 	echo ntsForm::wrapInput(
		M('How many meetings have you had with this student?'),
		$this->buildInput (
		/* type */
			'text',
		/* attributes */
			array(
				'id'	=> 'num_meetings', 
                        
				)
                
			
			)			
		);
    
	?>


<? 	//echo ntsForm::wrapInput(
	//	M('How satisfied are you with the tutors scheduling options and //procedure?'),
	//	$this->buildInput (
		/* type */
	//		'textarea',
		/* attributes */
	//		array(
	//			'id'	=> 'satisfied', 
                        
	//			)
                
			
	//		)			
	//	);
    
	?>



<? //	echo ntsForm::wrapInput(
//		M('How satisfied are you with the tutors knowledge of the material?'),
//		$this->buildInput (
		/* type */
//			'textarea',
		/* attributes */
//			array(
	//			'id'	=> 'satisfied_material', 
                        
	//			)
                
			
	//		)			
	//	);
    
	?>

<? //	echo ntsForm::wrapInput(
	//	M('How satisfied are you with the tutors teaching ability?'),
	//	$this->buildInput (
	/* type */
	//		'textarea',
		/* attributes */
		//	array(
		//		'id'	=> 'satisfied_ability', 
                        
		//		)
                
			
	//		)			
	//	);
    
	?>


<? //	echo ntsForm::wrapInput(
//		M('How satisfied are you with the tutors personal engagement?'),
	//	$this->buildInput (
		/* type */
	//		'textarea',
		/* attributes */
	//		array(
	//			'id'	=> 'satisfied_personal', 
                        
	//			)
                
			
	//		)			
	//	);
    
	?>


<? 	echo ntsForm::wrapInput(
		M('Please rate your overall satisfaction with the student?'),
		$this->buildInput (
		/* type */
			'textarea',
		/* attributes */
			array(
				'id'	=> 'satisfied_overall', 
                        
				)
                
			
			)			
		);
    
	?>


<? 	echo ntsForm::wrapInput(
		M('Comments'),
		$this->buildInput (
		/* type */
			'textarea',
		/* attributes */
			array(
				'id'	=> 'comments', 
                        
				)
                
			
			)			
		);
    
	?>







<?php if( NTS_ENABLE_TIMEZONES > 0 ) : ?>
	<?php if( $className == 'customer' ) : ?>
		<?php
		$timezoneOptions = ntsTime::getTimezones();
		echo ntsForm::wrapInput(
			M('Timezone'),
			$this->buildInput (
			/* type */
				'select',
			/* attributes */
				array(
					'id'		=> '_timezone',
					'options'	=> $timezoneOptions,
					)
				)
			);
		?>
	<?php endif; ?>
<?php endif; ?>

<?php echo $this->makePostParams('-current-', 'update'); ?>
<?php
echo ntsForm::wrapInput(
	'',
	'<INPUT class="btn btn-default btn-blue" TYPE="submit" VALUE="' . M('Send') . '">'
	);
?>

<?php if( NTS_ALLOW_NO_EMAIL ) : ?>
<script language="JavaScript">
jQuery(document).ready( function()
{
	if( jQuery("#<?php echo $this->getName(); ?>noEmail").is(":checked") )
	{
		jQuery("#<?php echo $this->getName(); ?>email").hide();
	}
	else
	{
		jQuery("#<?php echo $this->getName(); ?>email").show();
	}
});
jQuery("#<?php echo $this->getName(); ?>noEmail").live( 'click', function()
{
	jQuery("#<?php echo $this->getName(); ?>email").toggle();
});
</script>
<?php endif; ?>