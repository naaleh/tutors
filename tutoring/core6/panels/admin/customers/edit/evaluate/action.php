<?php
$ntsdb =& dbWrapper::getInstance();
$ff =& ntsFormFactory::getInstance();
$cm =& ntsCommandManager::getInstance();

$id = ntsLib::getCurrentUserId();

switch( $action ){
	case 'update':
		$formFile = dirname( __FILE__ ) . '/form';

		$object = new ntsUser();
		$object->setId( $id );
		$customerInfo = $object->getByArray();
		$customerInfo['object'] = $object;

		$form =& $ff->makeForm( $formFile, $customerInfo );

		$removeValidation = array();
		if( NTS_ALLOW_NO_EMAIL && $_NTS['REQ']->getParam('noEmail') ){
			$removeValidation[] = 'email';
			}

		if( $form->validate($removeValidation) ){
			$formValues = $form->getValues();
			if( isset($formValues['noEmail']) && $formValues['noEmail'] )
				$formValues['email'] = '';

		 //send email
          

              $output = "Student Name: " . $formValues['student_name'] . "\n\n"; 
              $output .= "Tutor Name: " . $formValues['tutor_name'] . "\n\n"; 
              $output .= "Date: " . $formValues['date'] . "\n\n"; 
              $output .= "Num of Meetings: " . $formValues['num_meetings'] . "\n\n"; 
              $output .= "Satisfaction with scheduling options and procedure: " . $formValues['satisfied'] . "\n\n"; 
              $output .= "Satisfaction with the tutors knowledge of the material: " . $formValues['satisfied_material'] . "\n\n"; 
              $output .= "Satisfaction with the tutors teaching ability: " . $formValues['satisfied_ability'] . "\n\n"; 
              $output .= "Satisfaction with the tutors personal engagement: " . $formValues['satisfied_personal'] . "\n\n"; 
              $output .= "Overall satisfaction with the tutor: " . $formValues['satisfied_overall'] . "\n\n"; 
              $output .= "Comments: " . $formValues['comments'] . "\n\n"; 

          	  //$output = http_build_query($formValues,'','            ');
              

              mail("leorawaldman@gmail.com", "NTT Evaluation", $output);


			$cm->runCommand( $object, 'update' );
			if( $cm->isOk() ){
				ntsView::setAnnounce( M('Thank you, your evaluation has been submitted') );

			/* continue to the list with anouncement */
				$forwardTo = ntsLink::makeLink( '-current-' );
				ntsView::redirect( $forwardTo );
				exit;
				}
			else {
				$errorText = $cm->printActionErrors();
				ntsView::addAnnounce( $errorText, 'error' );
				}
			}
		else {
		/* form not valid, continue to edit form */
			}
		break;

	case 'update_password':
		$ff =& ntsFormFactory::getInstance();
		$passwordFormFile = dirname( __FILE__ ) . '/passwordForm';
		$passwordForm =& $ff->makeForm( $passwordFormFile, array('id' => $id) );

		if( $passwordForm->validate() ){
			$cm =& ntsCommandManager::getInstance();
			$formValues = $passwordForm->getValues();

		/* update password */
			$user = new ntsUser();
			$user->setId( $id );
			$user->setProp( 'new_password', $formValues['password'] );

			$cm->runCommand( $user, 'update' );
			if( $cm->isOk() ){
				ntsView::addAnnounce( M('Password') . ': ' . M('Update') . ': ' . M('OK'), 'ok' );

			/* continue to customer edit */
				$forwardTo = ntsLink::makeLink( '-current-' );
				ntsView::redirect( $forwardTo );
				exit;
				}
			else {
				$actionError = true;
				$errorString = $cm->printActionErrors();
				}
			}
		else {
		/* form not valid, continue to edit form */
			}
		break;
	}

/* customer info */
$object = new ntsUser();
$object->setId( $id );

$customerInfo = $object->getByArray();
$customerInfo['object'] = $object;

if( ! isset($form) ){
	$formFile = dirname( __FILE__ ) . '/form';
	$form =& $ff->makeForm( $formFile, $customerInfo );
	}

if( ! isset($passwordForm) ){
	$passwordFormFile = dirname( __FILE__ ) . '/passwordForm';
	$passwordForm =& $ff->makeForm( $passwordFormFile, array('id' => $id) );
	}
?>