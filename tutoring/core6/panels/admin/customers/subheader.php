<?php
$returnTo = ntsLib::getVar('admin::returnTo');
?>
<h2>
	<?php if( $returnTo ) : ?>
		<?php echo M('Student'); ?>: <?php echo M('Select'); ?>
	<?php else : ?>
		<i class="fa fa-user"></i> <?php echo M('Students'); ?>
	<?php endif; ?>
</h2>