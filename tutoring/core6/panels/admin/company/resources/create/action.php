<?php
$ff =& ntsFormFactory::getInstance();
$formFile = NTS_APP_DIR . '/app/forms/resource';
$NTS_VIEW['form'] =& $ff->makeForm( $formFile );

switch( $action ){
	case 'save':
		if( $NTS_VIEW['form']->validate() ){
			$formValues = $NTS_VIEW['form']->getValues();

			$cm =& ntsCommandManager::getInstance();

		/* resource */
			$object = ntsObjectFactory::get( 'resource' );
			$object->setByArray( $formValues );
                  
                  
                  $cm->runCommand( $object, 'create' );
			$newObjId = $object->getId();

                 

                  //added by Leora
                  if(!empty($_FILES["nts-picture"]["name"])) {
                        
                       
                        $target_dir = "uploads/";
                  	$target_file = $target_dir . "tutorphoto_" . $newObjId;
                        
                                               
                  	move_uploaded_file($_FILES["nts-picture"]["tmp_name"], $target_file);
                       

                 		$im = new imagick($target_file);
                        $im->setImageFormat('png');
				$imageprops = $im->getImageGeometry();
				$width = $imageprops['width'];
   				$height = $imageprops['height'];
				if($width > $height){
    					$newHeight = 80;
   					 $newWidth = (80 / $height) * $width;
				}else{
    					$newWidth = 80;
    					$newHeight = (80 / $width) * $height;
				}                      
				$im->resizeImage($newWidth,$newHeight, imagick::FILTER_LANCZOS, 0.9, true);
				$im->cropImage (80,80,0,0);
				$im->writeImage($target_file .".png");
                        $im->clear();
				$im->destroy();
                   }
                  //end added by Leora


		/* assign all rights for the creating user */
			global $NTS_CURRENT_USER;
			$resourceSchedules = $NTS_CURRENT_USER->getSchedulePermissions();
			$resourceApps = $NTS_CURRENT_USER->getAppointmentPermissions();

			$resourceSchedules[ $newObjId ] = array( 'view' => 1, 'edit' => 1 );
			$resourceApps[ $newObjId ] = array( 'view' => 1, 'edit' => 1, 'notified' => 1 );

			$NTS_CURRENT_USER->setSchedulePermissions( $resourceSchedules );
			$NTS_CURRENT_USER->setAppointmentPermissions( $resourceApps );
			$cm->runCommand( $NTS_CURRENT_USER, 'update' );

			if( $cm->isOk() ){
				$msg = array( M('Bookable Resource'), ntsView::objectTitle($object), M('Create'), M('OK') );
				$msg = join( ': ', $msg );
				ntsView::addAnnounce( $msg, 'ok' );

			/* continue to the list with anouncement */
				$forwardTo = ntsLink::makeLink( '-current-/../browse' );
				ntsView::redirect( $forwardTo );
				exit;
				}
			else {
				$errorText = $cm->printActionErrors();
				ntsView::addAnnounce( $errorText, 'error' );
				}
			}
		else {
		/* form not valid, continue to create form */
			}

		break;
	default:
		break;
	}
?>