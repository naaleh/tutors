<?php
$ff =& ntsFormFactory::getInstance();
$object = ntsLib::getVar( 'admin/company/resources/edit::OBJECT' );

$formParams = $object->getByArray();
$formParams['object'] = $object;
$formFile = NTS_APP_DIR . '/app/forms/resource';
$NTS_VIEW['form'] =& $ff->makeForm( $formFile, $formParams );


switch( $action ){
	case 'save':
		if( $NTS_VIEW['form']->validate() ){
                 
			$formValues = $NTS_VIEW['form']->getValues();

                  //print_r($_FILES);
                 // $formFile = dirname( __FILE__ ) . '/form';
                 // echo $formFile; exit;
                //print_r($formValues);
                  if($formValues['deletepicture'] == "1") {
                  	//$formValues['picture'] = "";
                       // $formValues['deletepicture'] = 0;
                  }
                 
                  $object->setByArray( $formValues );
                   //print_r ($object->props['pictureee']['name']); exit;
                  $object->updatedProps['picture'] = $object->props['picture']['name'];
                  
                  
                  
                  if(!empty($_FILES["nts-picture"]["name"])) {
                        
                       
                  	$target_file = $target_dir . basename($_FILES["nts-picture"]["name"]);
                  	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

                        
                 
                  	$target_dir = "uploads/";
 
                 		//$target_file = $target_dir . "photo_" . $object->props['id'] . $_FILES["nts-picture"]["name"];
                        $target_file = $target_dir . "tutorphoto_" . $object->props['id'];

                         
                                               
                  	move_uploaded_file($_FILES["nts-picture"]["tmp_name"], $target_file);
                         
                           
                 		$im = new imagick($target_file);

                       

                        $im->setImageFormat('png');
				$imageprops = $im->getImageGeometry();
				$width = $imageprops['width'];
   				$height = $imageprops['height'];
				if($width > $height){
    					$newHeight = 80;
   					 $newWidth = (80 / $height) * $width;
				}else{
    					$newWidth = 80;
    					$newHeight = (80 / $width) * $height;
				} 

                                           
				$im->resizeImage($newWidth,$newHeight, imagick::FILTER_LANCZOS, 0.9, true);
				$im->cropImage (80,80,0,0);
				$im->writeImage($target_file .".png");
                        $im->clear();
				$im->destroy();
                   }
                  
            
			$cm =& ntsCommandManager::getInstance();
			$cm->runCommand( $object, 'update' );

			if( $cm->isOk() ){
				$msg = array( M('Bookable Resource'), ntsView::objectTitle($object), M('Update'), M('OK') );
				$msg = join( ': ', $msg );
				ntsView::addAnnounce( $msg, 'ok' );
				
			/* continue to the list with anouncement */
				$forwardTo = ntsLink::makeLink( '-current-/../../browse' );
				ntsView::redirect( $forwardTo );
				exit;
				}
			else {
				$errorText = $cm->printActionErrors();
				ntsView::addAnnounce( $errorText, 'error' );
				}
			}
		else {
		/* form not valid, continue to create form */
			}
		break;

	default:
		break;
	}
?>