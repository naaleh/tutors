<?php
$object = ntsLib::getVar( 'admin/manage/appointments/edit::OBJECT' );

$ntspm =& ntsPaymentManager::getInstance();
$cm =& ntsCommandManager::getInstance();

$coupon = $_NTS['REQ']->getParam('coupon');

/* now check if we can apply promotions  */
$promotions = $ntspm->getPromotions( $object, $coupon );
foreach( $promotions as $pro )
{
	$cm->runCommand( 
		$pro,
		'apply',
		array(
			'appointment'	=> $object,
			'coupon'		=> $coupon,
			)
		);
}

if( $cm->isOk() ){
	}
else {
	$errorText = $cm->printActionErrors();
	ntsView::addAnnounce( $errorText, 'error' );
	}

$forwardTo = ntsLink::makeLink( '-current-' );
ntsView::redirect( $forwardTo );
exit;
?>