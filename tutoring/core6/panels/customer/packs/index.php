<?php
$aam =& ntsAccountingAssetManager::getInstance();
?>
<h1 id="page_header">Packages</h1>
	<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>

<div class="wrap w-inner">

<?php if( ! $packs ) : ?>
	<p>
	<?php echo M('Not Available'); ?>
	</p>
	<?php return; ?>
<?php endif; ?>
<?php
$current_user = ntsLib::getCurrentUser();
$tutor = ntsObjectFactory::get('resource');
$tutor->setId($current_user->props['_assign_resource']);?>
<?php $myTutorPack = false;

//echo $tutor->props['title'];
?>
<?php foreach( $packs as $e ) : 
$asset_view = $aam->asset_view( $e->getProp('asset_id'), TRUE, array('location', 'resource', 'service') );
if($asset_view)
    foreach( $asset_view as $av )
       foreach ($av[1] as $av2 )
            if($av2 == $tutor->props['title']){
              $myTutorPack = true;
              break;
            }
if(!$asset_view || $myTutorPack)
{
?>
	<div class="collapse-panel panel panel-package">
		<div class="panel-heading-package">
			<div class="text-strong text-success">
				<?php echo ntsCurrency::formatPrice($e->getProp('price')); ?> 
			</div>
			<h4 class="panel-title-package">
				<a href="#" data-toggle="collapse-next">
					<?php echo ntsView::objectTitle($e); ?>
				</a>
			</h4>
		</div>

		<div class="panel-collapse collapse in">
			<div class="panel-body">
				<dl class="dl-horizontal">
					<dt>
						<?php echo M('Value'); ?>
					</dt>
					<dd>
						<?php
						echo $aam->format_asset(
							$e->getProp('asset_id'),
							$e->getAssetValue(),
							TRUE,
							FALSE
							);
						?>
					</dd>

					<?php
					$asset_view = $aam->asset_view( $e->getProp('asset_id'), TRUE, array('location', 'resource', 'service') );
					?>
					<?php if( $asset_view ) : ?>
						<dt>
							<?php echo M('Valid For'); ?>
						</dt>
						<dd>
							<ul class="list-unstyled">
							<?php foreach( $asset_view as $av ) : ?>
								<li>
									<ul class="list-inline">
										<li style="vertical-align: top;">
											<?php echo $av[0];?>
										</li>
										<li>
											<ul class="list-unstyled">
												<?php foreach( $av[1] as $av2 ) : ?>
													<li>
														<?php 
														echo $av2; ?>
													</li>
												<?php endforeach; ?>
											</ul>
										</li>
									</ul>
								</li>
							<?php endforeach; ?>
							</ul>
						</dd>
					<?php endif; ?>
				
					<?php
					$expires_in = $e->getProp('expires_in');
					?>
					<?php if( $expires_in ) : ?>
						<?php
						$this_view = '';
						list( $qty, $measure ) = explode( ' ', $expires_in );
						$this_view .= $qty;
						$tag = ( $qty > 1 ) ? $measure : substr($measure, 0, -1);
						$tag = ucfirst( $tag );
						$this_view .= ' ' . M($tag);
						?>
						<dt>
							<?php echo M('Expires In'); ?>
						</dt>
						<dd>
							<?php echo $this_view; ?>
						</dd>
					<?php endif; ?>
				</dl>

			</div>

			<div class="panel-footer">
				<ul class="list-inline">
					<li class="text-strong text-success">
						<?php echo ntsCurrency::formatPrice($e->getProp('price')); ?> 
					</li>
					<li id="buy">
						<a class="btn btn-default" href="<?php echo ntsLink::makeLink('-current-/confirm', '', array('pack' => $e->getId())); ?>">
							<?php echo M('Purchase'); ?>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php }endforeach; ?>
</div><!--end wrap-->