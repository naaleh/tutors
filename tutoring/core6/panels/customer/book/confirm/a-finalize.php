<?php
include_once( dirname(__FILE__) . '/zoom.php' );

$cm =& ntsCommandManager::getInstance();
$tm2 = $NTS_VIEW['tm2'];



$session = new ntsSession;
$apps = $session->userdata('apps');
//print_r($apps); exit;

$coupon = $session->userdata('coupon');
//echo count($apps); exit;
require( dirname(__FILE__) . '/_check.php' );

$ready = array();
$group_ref = '';
if( count($apps) > 1 ){
	$object = ntsObjectFactory::get( 'appointment' );
	$group_ref = $object->generate_group_ref();
}

for( $ii = 0; $ii < count($apps); $ii++ ){
	$apps[$ii]['group_ref'] = $group_ref;
      $start_time = $apps[$ii]['starts_at'];  
     	$customer_id = $apps[$ii]['customer_id'];
      $tutor_id = $apps[$ii]['resource_id'];

       $student = ntsObjectFactory::get('user');
       $student->setId($customer_id);
       //print_r($student); exit;
       $timezone = $student->props['_timezone']; 
       $student_name = $student->props['first_name'] . " " . $student->props['last_name']; 
    

       date_default_timezone_set('utc');
       $start_time = date('Y-m-d\TH:i:s\Z',$start_time);
             

       $tutor = ntsObjectFactory::get('resource');
      
	 $tutor->setId($tutor_id);      
       $tutor_email = $tutor->props['email'];
    
      $meeting = new ZoomAPI();
      $tutoring_session = $meeting->createAMeeting($tutor_email, $start_time, $timezone, $student_name);
      $tutoring_info = json_decode($tutoring_session, true);
      $zoom_url =  $tutoring_info['join_url'];
      

      $apps[$ii]['custom_zoom_url'] = $zoom_url;
     // print_r($apps[$ii]); exit;
      //echo ("in here"); exit;
	$cm->act_as = $customer_id;

	$object = ntsObjectFactory::get( 'appointment' );
      
	$object->setByArray( $apps[$ii] );
      //print_r($object); exit;

	$params = array(
		'coupon'	=> $coupon
		);
	$cm->runCommand( $object, 'init', $params );

	if( $cm->isOk() ){
		$cm->runCommand( $object, '_request' );
		$ready[] = $object;
	}
	else {
		$errorText = $cm->printActionErrors();
		ntsView::addAnnounce( $errorText, 'error' );
	}
}

if( count($ready) ){
     
	if( count($ready) > 1 ){
		$add_msg = join( ': ', array( count($ready), M('Appointments'), M('Created') ) );
	}
	else {
		$add_msg = join( ': ', array( M('Appointment'), M('Created') ) );
	}
	//ntsView::addAnnounce( $add_msg, 'ok' );

	$ref = $ready[0]->getProp( 'group_ref' );
	$forwardTo = ntsLink::makeLink('customer/appointments/view', '', array('ref' => $ref, 'action' => 'pay'));
}
else {
       
	$forwardTo = ntsLink::makeLink('-current-/..');
      //$forwardTo = ntsLink::makeLink('customer/appointments/view', '', array('ref' => $ref, 'action' => 'pay'));

}

/* clear cart */
$session->sess_destroy();



ntsView::redirect( $forwardTo );
exit;
?>