<?php
$session = new ntsSession;
$apps = $session->userdata( 'apps' );
?>
<h1 id="page_header">Confirm Appointment</h1>
	<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
 <div class="wrap w-inner">
	<h2>
	<?php if( $apps ) : ?>
		<?php echo M('Please confirm your appointment.'); ?>
	<?php endif; ?>
	</h2>
<div id="app_confirm">

<?php require( dirname(__FILE__) . '/../_index_confirm.php' ); ?>



<?php echo $NTS_VIEW['form']->display(); ?>

</div></div>