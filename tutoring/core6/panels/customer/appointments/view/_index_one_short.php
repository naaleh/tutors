<?php
$conf =& ntsConf::getInstance();
$auto_resource = $conf->get('autoResource');
$auto_location = $conf->get('autoLocation');

$actions = array();

$completedStatus = $a->getProp('completed');
$approvedStatus = $a->getProp('approved');

$service = ntsObjectFactory::get( 'service' );
$service->setId( $a->getProp('service_id') );

$displayColumns = array();
if( ! (NTS_SINGLE_LOCATION OR $auto_location) )
	$displayColumns[] = 'location';
if( ! (NTS_SINGLE_RESOURCE OR $auto_resource) )
	$displayColumns[] = 'resource';

/* due payment */
$cost = $a->getCost();
$payment_options = array();
$default_payment_option = '';

/* get any promotions applied for this appointment */
$promotions = array();
$am =& ntsAccountingManager::getInstance();
$acc_postings = $am->get_postings( $a->getClassName(), $a->getId() );
foreach( $acc_postings as $ap ){
	$promotion = NULL;
	switch( $ap['obj_class'] ){
		case 'promotion':
			$promotion = ntsObjectFactory::get('promotion');
			$promotion->setId( $ap['obj_id'] );
			break;
		case 'coupon':
			$coupon = ntsObjectFactory::get('coupon');
			$coupon->setId( $ap['obj_id'] );
			$promotion = ntsObjectFactory::get('promotion');
			$promotion->setId( $coupon->getProp('promotion_id') );
			break;
	}
	if( $promotion ){
		$promotions[] = $promotion;
	}
}

if( $cost )
{
	$due = $a->getDue();
	$paid = $a->getPaidAmount();

	$balance_cover = array();
	if( $customer_balance )
	{
		$balance_cover = $am->balance_cover( $customer_balance, $a );
	}
	$displayColumns[] = 'payment';

	if( $group_ref )
	{
		require( dirname(__FILE__) . '/_payment_group.php' );
	}
	else
	{
		require( dirname(__FILE__) . '/_payment_browse.php' );
	}
}
?>

<?php
	$minCancel = $service->getProp('min_cancel');
      
	if( ($now + $minCancel) > $a->getProp('starts_at') )
	{
		// can't cancel
	}
	else
	{
		
		     
			$actions[] = array(
				ntsLink::makeLink('-current-/../edit/cancel', '', array('_id' => $a->getId(), NTS_PARAM_RETURN => 'all') ),
				'<i class="fa fa-times text-danger"></i> ' . M('Cancel'),
				);



				
		} ?>


                         

<!--<a href="http://www.theyeshivaworld.com"  onclick="window.open('http://www.theyeshivaworld.com')" >Launch Tutoring App</a>
<a href="<? echo $a->getProp('custom_zoom_url'); ?>"  onclick="window.open('<? echo $a->getProp('custom_zoom_url'); ?>')>Launch Tutoring App</a>-->

<? $t->setTimestamp( $a->getProp('starts_at') );

$dateView = $t->formatDateFull();
$fullDateView = '<i class="fa fa-fw fa-calendar"></i>' . $dateView;

$timeView = $t->formatTime();
$fullTimeView = '<i class="fa fa-fw fa-clock-o"></i>' . $timeView;

$status_class = $a->statusClass();
$status_text = $a->statusText();

$collapse_in = $group_ref ? ' in' : '';
$collapse_in = ' in';

$app_seats = $a->getProp('seats');

?>
<li class="collapse-panel panel panel-default panel-<?php echo $status_class; ?>">
	<div class="panel-heading" title="<?php echo $status_text; ?>">
		<span class="xs-block"><?php echo $fullDateView; ?></span>
		<span class="xs-block"><?php echo $fullTimeView; ?></span>
		<span class="xs-block"><?php echo ntsView::objectTitle( $service, TRUE ); ?></span>
            <? if($now <= $a->getProp('starts_at')) { ?>
                   				 
            	                 
<a class="nts-no-ajax" href="<? echo $a->getProp('custom_zoom_url'); ?>" target="_blank">Launch Tutoring App</a>
            <? } ?>
            <?php foreach( $actions as $aa ) : ?>
						<?php
						list( $action_title, $action_icon ) = Hc_lib::parse_icon($aa[1]);
						?>
						<span class="xs-block">
							<a href="<?php echo $aa[0]; ?>" class="btn btn-default" title="<?php echo $action_title; ?>">
								<?php echo $action_icon; ?><?php echo $action_title; ?>
							</a>
						</span>
					<?php endforeach; ?>

	</div>
</li>
