<h1 id="page_header">Evaluation Form</h1>
	<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
<div class="wrap inner_pages">
<h3>Naaleh Torah Tutors strives to provide excellent tutors who combine the teaching skills needed with a warm and enjoyable atmosphere. We appreciate your feedback. </h3>

<?php
$ff =& ntsFormFactory::getInstance();
$form =& $ff->makeForm( dirname(__FILE__) . '/form' );
$form->display();
?>



</div>