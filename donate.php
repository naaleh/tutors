
<!DOCTYPE html>
<html lang="en-GB">
<head itemscope itemtype="http://schema.org/WebSite">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naaleh Torah Tutors: Donate</title>
<meta name="description" content="Help us help children succeed in their Torah learning. Your scholastic scholarship will provide support for those who require tutoring but can't afford it." />



<?php include('inc_files/header.inc'); ?>
<div class="full-width-content donate">         
<div class="site-inner">

	<h1 id="page_header"><i class="fa fa-usd" aria-hidden="true"></i> Donate</h1>
		<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>	

<?php
//here is the form to send to authorizenet

if( ! class_exists('AuthorizeNetSim') ){
	require_once( 'tutoring/core6/payment/authorizenet/code/AuthorizeNetSim.php' );
	}
$authorizeNetSim = new AuthorizeNetSim;

$loginId = '8r2TCh75';
$tranKey = '7L5BYmt5d3773yB2';
//$paymentAmount='';
$paymentCurrency='usd';
//$customerEmail = $invoiceInfo['customer']->getProp( 'email' );

srand(time());
$sequence = rand(1, 1000);
?>

<div>

</div>

<div class="wrap">
<h3>Help us help children succeed in their Torah learning.</h3>
<h4>How much can you donate?</h4>
<?php

?>
<div>
    <form action="" method="POST">
        <label>$</label>
        <input type="text" name="amount" id="amount" style="width:100px"value="<?php echo isset($_POST['amount']) ? $_POST['amount'] : '' ?>"><br>
        <input class="button" type="submit" value="OK">
    </form>
</div>

<div >
    <?php
    $paymentAmount = $_POST['amount'];

    if($paymentAmount>0){ 
    ?>
    <h3>You are donating <?php echo '$'. $paymentAmount;?></h3>

 <ul class="list-inline">
     <li>
         <FORM ACTION="https://secure.authorize.net/gateway/transact.dll" METHOD=POST>
<?php $authorizeNetSim->InsertFP($loginId, $tranKey, $paymentAmount, $sequence, $paymentCurrency); ?>
<INPUT type="hidden" name="x_show_form" value="PAYMENT_FORM">
<INPUT type="hidden" name="x_relay_response" value="FALSE">
<INPUT type="hidden" name="x_login" value="<?php echo $loginId; ?>">
<INPUT type="hidden" name="x_currency_code" value="<?php echo $paymentCurrency; ?>">
<INPUT type="hidden" name="x_amount" id="x_amount" value="<?php echo $paymentAmount; ?>">
<INPUT type="hidden" name="x_description" value="<?php echo 'Donation'; ?>">
<INPUT type="hidden" name="x_relay_url" value="<?php echo 'https://www.naaleh.com'; ?>">

<input type="hidden" name="x_receipt_link_url" value="<?php echo 'https://naalehtutors.com'; ?>">
<input type="hidden" name="x_receipt_link_text" value="Return to naalehtutors">
<input type="hidden" name="x_receipt_link_method" value="POST">
<input class="button" type="submit" value="Credit Card">

</FORM>
     </li>
     <li>
         <?php //-------------paypal

$paypalUrl = "https://www.paypal.com/cgi-bin/webscr"; // live
?>
<FORM ACTION="<?php echo $paypalUrl; ?>" METHOD=POST target="_parent">
<INPUT TYPE="hidden" name="cmd" value="_xclick">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="no_note" value="1">

<input type="hidden" name="business" value="<?php echo 'director@naaleh.com'; ?>">

<INPUT TYPE="hidden" NAME="currency_code" value="<?php echo strtoupper($paymentCurrency); ?>">
<input type="hidden" name="amount" value="<?php echo $paymentAmount; ?>">
<input type="hidden" name="item_name" value="<?php echo '$paymentItemName'; ?>">
<input type="hidden" name="item_number" value="<?php echo '$paymentOrderRefNo'; ?>">

<input type="hidden" name="return" value="https://naalehtutors.com">
<input type="hidden" name="cancel_return" value="https://naalehtutors.com/donate">
<input type="hidden" name="notify_url" value="<?php echo 'https://www.naaleh.com'; ?>">

<input class="button" type="submit" value="PayPal">

</FORM>
     </li>
 </ul>  

<?php }?>
</div>
<!--<a href="https://secure.authorize.net/gateway/transact.dll" class="button">Credit Card<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>

<a href="" class="button">PayPal<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>-->

<div class="image-line">

					<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
			<img src="images/donate_images.jpg"  no-repeat center />
							<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div></div>
		</div><!--end wrap-->
		</div><!--end inner content-->
		</div>
</div><!--end of inner-->
</div>
<?php include('inc_files/footer.inc'); ?>
