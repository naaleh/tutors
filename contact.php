<?


if(isset($_POST['submit'])) {
   
   $info = "";
   foreach($_POST as $data) {
        $data = trim($data);
  	  $data = stripslashes($data);
        $data = htmlspecialchars($data);
        $info .= $data . "<BR>";

   }
      $to = 'torahtutors@naaleh.com';
	$subject = 'NTT contact request';
      $from = $_POST['nts-email'];
 
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
 
// Create email headers
$headers .= 'From: '.$from."\r\n".
    'Reply-To: '.$from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
 
// Compose a simple HTML email message
$message = '<html><body>';
$message .= $info;
$message .= '</body></html>';
 
// Sending email
if(mail($to, $subject, $message, $headers)){
    $message = 'Your mail has been sent successfully.';
} else{
    $message = 'Unable to send email. Please try again.';
}

}

?>
<!DOCTYPE html>
<html lang="en-GB">
<head itemscope itemtype="http://schema.org/WebSite">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Contact Naaleh Torah Tutors</title>
<meta name="description" content="Contact us today and find out how we can help your child succeed." />


<?php include('inc_files/header.inc'); ?>           
<div class="site-inner">
	<h1 id="page_header"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us</h1>      
		<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
      <? if($message != "") { ?>
      	<div id="app_success"><? echo $message; ?> </div>
      <? } ?>	
	<div class="page_intro">
		<div class="wrap">
		<div id="intro-wrapper">
		<h3>We'd love to help!<br /> Feel free to contact us with any questions or comments!</h3>
			<ul>
				<li><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;1-646-493-7092
                </li>
				<li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="maito:torahtutors@naaleh.com">torahtutors@naaleh.com</a></li>
				<li><i class="fa fa-map-marker" aria-hidden="true"></i> 17 Fort George Hill Apt. 7J, New York, NY 10040</li>
			</ul>
		</div>
		</div><!--end wrap-->
	</div><!--end of page intro-->
	
	<div class="content-sidebar-wrap">
		<div class="inner_content">
			<div class="wrap">
				<div class="color-bar">
					<div class="color-block"></div>
					<div class="color-block"></div>
					<div class="color-block" id="color-block-3"></div>
					<div class="color-block" id="color-block-2"></div>
					<div class="color-block" id="color-block-1"></div>
				</div>
				<div id="contact_forms">
				<h4>Please choose an option below to complete a contact form:</h4>
                        <ul class="nav nav-pills">
                              <li class="active"><a data-toggle="pill" href="#menu1"><button data-toggle="collapse" data-target="#s-form">I'd like to know more about becoming a student</button></a></li>
    					<li><a data-toggle="pill" href="#home"><button data-toggle="collapse" data-target="#t-form">I'd like to know more about becoming a tutor</button></a></li>
    					
      			</ul>
				
				
					<div class="tab-content">

   					<div id="menu1" class="tab-pane fade in active">				
						<div id="student-form">
							<form data-toggle="validator" role="form" class="form-horizontal form-condensed form-striped" METHOD="post" ACTION="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" ENCTYPE="multipart/form-data" NAME="nts_form_g2spgm" ID="nts_form_g2spgm">
                                                      <input type="hidden" name="infostudent" value="I would like info about becoming a student">
									<div class="form-group f-half1"><label class="f-half1 control-label">First Name *</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_g2spgmfirst_name" NAME="nts-first_name" VALUE="" cols="" rows="" size="32" style="" data-error="First name is required" required></div>
                                                      	<div class="help-block with-errors"></div>
                                                      </div>
									<div class="form-group f-half2"><label class="f-half2 control-label">Last Name *</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_g2spgmlast_name" NAME="nts-last_name" VALUE="" cols="" rows="" size="32" style="" data-error="Last name is required" required></div>
                                                            <div class="help-block with-errors"></div>
                                                      </div>
									<div class="form-group"><label class="control-label">Phone *</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_g2spgmcustom_home_number" NAME="nts-custom_home_number" VALUE="" cols="" rows="" size="10" style="" data-error="Phone number is required" required></div>
									      <div class="help-block with-errors"></div>
                                                      </div>
									<div class="form-group"><label class="control-label">Email *</label><div class="control-holder"><INPUT class="form-control" type="email" TYPE="text" ID="nts_form_g2spgmemail" NAME="nts-email" VALUE="" cols="" rows="" size="32" style="" required data-error="A valid email is required"></div>
										<div class="help-block with-errors"></div>
                                                      </div>
									<div class="form-group"><label class="control-label">School </label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_g2spgmcustom_school" NAME="nts-custom_school" VALUE="" cols="" rows="" size="64" style=""></div></div>
									<div class="form-group"><label class="control-label">How can we help?</label><TEXTAREA class="form-control" ID="nts_form_g2spgmcustom_student_description" NAME="nts-custom_student_description" cols="30" rows="8" size=""></TEXTAREA></div>
									<button type="submit" class="btn btn-default" name="submit">Submit</button>
							</form>
						</div>
					</div>
                    
                              	<div id="home" class="tab-pane fade">
							<div id="tutor-form">
										<form data-toggle="validator" role="form" class="form-horizontal form-condensed form-striped" METHOD="post" ACTION="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" ENCTYPE="multipart/form-data" NAME="nts_form_g2spgm" ID="nts_form_g2spgm">
                                                                        <input type="hidden" name="info" value="I would like info about becoming a tutor">
												<div class="form-group f-half1"><label class="f-half1 control-label">First Name *</label><div class="control-holder"><INPUT class="form-control" id="inputSuccess1" TYPE="text"  NAME="nts-first_name" VALUE="" cols="" rows="" size="32" style="" data-error="First name is required" required></div>
                                                                        	<div class="help-block with-errors"></div>
                                                                        </div>
												<div class="form-group f-half2"><label class="f-half2 control-label">Last Name *</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_g2spgmlast_name" NAME="nts-last_name" VALUE="" cols="" rows="" size="32" style="" data-error="Last name is required" required></div>
													<div class="help-block with-errors"></div>
                                                                        </div>
												<div class="form-group"><label class="control-label">Phone *</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_g2spgmcustom_home_number" NAME="nts-custom_home_number" VALUE="" cols="" rows="" size="10" style="" data-error="Phone is required" required></div>
													<div class="help-block with-errors"></div>
                                                                        </div>
												<div class="form-group"><label class="control-label">Email *</label><div class="control-holder"><INPUT class="form-control" type="email" TYPE="text" ID="nts_form_g2spgmemail" NAME="nts-email" VALUE="" cols="" rows="" size="32" style="" required data-error="A valid email is required"></div>
													<div class="help-block with-errors"></div>
                                                                        </div>									
													
                                                                      																								<div class="form-group"><label class="control-label">How can we help?</label><TEXTAREA class="form-control" ID="nts_form_g2spgmcustom_student_description" NAME="nts-custom_student_description" cols="30" rows="8" size=""></TEXTAREA></div>
												<button type="submit" class="btn btn-default" name="submit">Submit</button>
										</form>
						    </div>
					</div>
                       </div>
			</div><!--end of contact_forms-->
						</div><!--end wrap-->
		</div><!--end inner content-->
	</div><!--end of content sidebar-wrap-->
</div><!--end of inner-->


<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/validator.min.js"></script>
<script src="http://platform.twitter.com/widgets.js"></script>
<script src="assets/js/application.js"></script>

<?php include('inc_files/footer.inc'); ?>
