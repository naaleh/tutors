
<!DOCTYPE html>
<html lang="en-GB">
<head itemscope itemtype="http://schema.org/WebSite">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naaleh Torah Tutors: Our Tutors</title>
<meta name="description" content="Our tutors are based in Israel and possess years of experience teaching and studying Torah. Our learning specialists have excellent credentials and will help your child succeed!" />


<?php 
$class = "tutors_pg";
include('inc_files/header.inc'); ?>          
<div class="site-inner">

	<h1 id="page_header"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Our Tutors</h1>
		<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>	
<div class="wrap">
	<div class="tutor_p">
		<div class="tutor-details rotate90" style="background-image:url('images/Rabbi-Doniel-Tisser.jpg');">
		</div>	
			<h2>Rabbi Doniel Tisser
			</h2>
		<p id="t_desc"><b>Rabbi Doniel Tisser</b>, a Torah teacher licensed by Torah Umesorah, has been learning with students aged 6-60 for over twenty years.  He has helped many to gain the skills they need to learn on their own. He has facilitated the success of students dealing with all sorts of learning issues, and has developed techniques for overcoming challenges that range from reading difficulties to ADHD (including his own). He uses creative teaching methods such as props and mountain biking to help bring Torah to life.  Rabbi Tisser enjoys learning the practical side of Torah, and he is a certified shochet and mohel. He has received Smicha from Yeshivas Ner Yisroel, Rav Reuven Feinstein shlit”a and the Chief Rabbinate of Israel and now serves on beis din Chuchat Mishpat in Beit Shemesh. Make learning fun with Rabbi Tisser!
        </p>
	</div>
	<div class="tutor_p">
		<div class="tutor-details" style="background-image:url('images/Mrs-Chana-Prero.jpg');">
		</div>	
			<!--<img class="aligncenter" src="images/Mrs-Chana-Prero.jpg" width="150" height="150">-->
			<h2>Mrs. Chana Prero
			</h2>
		<p id="t_desc"><b>Mrs. Chana Prero</b>, originally from New York,  is an energetic and creative teacher of Torah.  Mrs. Prero has been teaching Torah to small groups for the over 10 years.  She has taught both Torah and science in a variety of high schools and post-secondary schools.  Her Parsha shiurim are featured on Naaleh.com.  Mrs. Prero has a passion for parshanut, understanding commentaries.  She enjoys taking a Torah verse and making it come alive for her students.  She lives in Mevo Choron, Israel, with her husband and five children.
        </p>
	</div>
	<div class="tutor_p">
		<div class="tutor-details" style="background-image:url('images/Rabbi-Yosef-Bronstein.jpg');">
		</div>	
			<h2>Rabbi Yosef Bronstein
			</h2>
		<p id="t_desc"><b>Rabbi Yosef Bronstein</b> resides in Ramat Bet Shemesh with his wife, Batya, and children, Talya, Yehuda and Yonatan.  A recent Oleh, he teaches Halacha and Jewish Philosophy at Michlelet Mevaseret Yerushalayim and Nishmat, while pursuing a PhD in Talmudic Studies at Yeshiva University’s Bernard Revel Graduate School of Jewish Studies.  Before making aliyah, he was a professor of Jewish Philosophy at Yeshiva University’s Stern College for Women and Isaac Breuer College of Hebraic Studies Honors Program.  He received semikha from Yeshiva University’s RIETS, an MA in Talmudic Studies from Revel, and completed RIET’s Wexner Kollel Elyon.  Most importantly, his favorite color is purple.
        </p>
	</div>
</div><!--end wrap-->
<div id="b-tutor">
<h3>Become a tutor, make a difference <a href="/./contact.php" class="button">Learn More</a></h3> 
</div>
<div class="wrap">
<div id="t-exp"">
<h2 class="section_title first"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Our Standards:</h2>
			<p>  <em class="p_start"> Naaleh Torah Tutors maintains the highest level </em>of standards for our tutors. Each tutor is interviewed and receives a thorough review of his or her qualifications and experience. Our tutors understand and can relate to kids in America.
			</p>
</div>			
</div><!--end wrap-->
</div><!--end inner content-->
</div><!--end of container-->
</div><!--end of inner-->

<?php include('inc_files/footer.inc'); ?>
