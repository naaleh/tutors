<footer class="site-footer">
		<div class="wrap">
			<div class="logo-footer one-half first">
            	<h3>Naaleh Torah tutors</h3>
				<ul>
            		<li>17 Fort George Hill Apt. 7J </li>
					<li>New York, NY 10040</li>
					<li><a href="maito:torahtutors@naaleh.com">torahtutors@naaleh.com</a></li>
             	</ul>	
			</div>
			
			<div class="footer-info">
				<p> Naaleh Torah Tutors is sponsored<br /> through a grant from AVI CHAI</p>
                <img src="" alt="" />
			</div>
		</div>
</footer>
</div>
</body></html>
