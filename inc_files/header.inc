<?php
 //echo '$_SERVER[\'HTTPS\']: '. $_SERVER['HTTPS'];
 //echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
//echo "here we are";
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
   //$redirect = 'https://www.google.com';
    header('HTTP/1.1 301 Moved Permanently');
    header('Location:' . $redirect);
    exit();
}
?>

<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Arima+Madurai" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Delius" rel="stylesheet">
<script src="https://use.fontawesome.com/ca32fe49e5.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/smoothscroll.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel='stylesheet' id='css'  href='style.css' type='text/css' media='all' />

</head>
<body class="<? echo $class; ?>" full-width-content">
<div class="site-container">
	<header class="site-header">
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Login to Naaleh Torah Tutors</h4>
            </div>
            <div class="modal-body">
     <FORM METHOD="post" ACTION="/./tutoring/" ENCTYPE="multipart/form-data" NAME="nts_form_l7xvoz" ID="nts_form_l7xvoz">
	<div class="form-group"><label class="control-label">Username</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_l7xvozusername" NAME="nts-username" VALUE="" size="20" style=""></div></div>
<div class="form-group"><label class="control-label">Password</label><div class="control-holder"><INPUT class="form-control" TYPE="password" ID="nts-password" NAME="nts-password" VALUE="" size="20" style=""></div></div>
<div class="form-group"><div class="control-holder"><div class="checkbox"><label><INPUT TYPE="checkbox" ID="nts_form_l7xvozremember" NAME="nts-remember"> Remember Me</label></div></div></div>
<INPUT TYPE="hidden" NAME="nts-panel" VALUE="anon/login">
<INPUT TYPE="hidden" NAME="nts-action" VALUE="login"><div class="form-group"><label class="control-label"></label><div class="control-holder"><p class="form-control-static"><INPUT class="links" TYPE="submit" VALUE="Login"></p></div></div></FORM>
<div class="form-horizontal collapse-panel">
<div id="form-bottom">	
	<div class="form-group"><label class="control-label"></label><div class="control-holder"><p class="form-control-static"><a href="/./tutoring" data-toggle="collapse-next">Forgot Your Password?</a></p></div></div>	<div class="collapse">
	
<FORM class="form-horizontal form-condensed form-striped" METHOD="post" ACTION="https://naalehtutors.com/tutoring/" ENCTYPE="multipart/form-data" NAME="nts_form_dxtmcj" ID="nts_form_dxtmcj">
<div class="form-group"><label class="control-label">Email</label><div class="control-holder"><INPUT class="form-control" TYPE="text" ID="nts_form_dxtmcjemail" NAME="nts-email" VALUE="" size="20" style=""></div></div>
<INPUT TYPE="hidden" NAME="nts-panel" VALUE="anon/login">
<INPUT TYPE="hidden" NAME="nts-action" VALUE="reset">
<div class="form-group"><div class="control-holder"><p class="form-control-static"><INPUT class="btn btn-default" TYPE="submit" VALUE="Send New Password"></p></div></div></FORM>	</div>
</div>

	<div class="form-horizontal">
		<div class="form-group"><div class="control-holder"><p class="form-control-static"><a href="https://naalehtutors.com/tutoring/?nts-panel=anon%2Fregister">New to our site? Please take a moment to register!?</a></p></div></div>	</div>
           </div><!--end bottom--></div>
        </div>
    </div>
</div><!--end of modal-->
 <nav class="navbar navbar-fixed-top nav-primary">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand logo" href="/./"><img src="images/logo.jpg" alt="Naaleh Torah Tutors" /><h1 id="site-title">Naaleh <br />Torah Tutors</h1></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav" id="menu-main">
        <li id="menu-item-1"><a href="/./about.php"><i class="fa fa-users" aria-hidden="true"></i><br />About Us</a></li>
        <li id="menu-item-1"><a href="<?php echo (basename($_SERVER['PHP_SELF'])=='index.php'?'#works' : '/./index.php#works'); ?>"><i class="fa fa-cogs" aria-hidden="true"></i><br />How it works</a></li>
        <li id="menu-item-1"><a href="/./tutors.php"><i class="fa fa-user-circle-o" aria-hidden="true"></i><br />Our Tutors</a></li>
        <li id="menu-item-1"><a href="/./donate.php"><i class="fa fa-usd" aria-hidden="true"></i><br />Donate</a></li>
        <li id="menu-item-1"><a href="/./contact.php"><i class="fa fa-phone" aria-hidden="true"></i><br />Contact</a></li>
      </ul>
	  <ul class="nav navbar-nav navbar-right" id="right_menu">
        <li><a href="#" data-toggle="modal" data-target="#basicModal">Login</a></li>
        <li><a href="/./tutoring/?nts-panel=anon%2Fregister">Sign Up</a></li>
      </ul>
    </div>
  </div>
</nav>
         
               </header>