<?php 
include('inc_files/header.inc');
?>
<!DOCTYPE html>
<html lang="en-GB">
<head itemscope itemtype="http://schema.org/WebSite">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naaleh Torah Tutors: Online Jewish Studies Tutoring</title>
<meta name="description" content="Naaleh Torah Tutors is a personalized and affordable online Jewish Studies tutoring platform for children grades K-12" />


<?php 
$class = "home";
 ?>

<div class="site-inner">	
<div id="hero-slider">
<div class="hero-container">
    <div class="one-half first">
				<div class="wrap">
                    <h1>Online Jewish Studies Tutoring</h1>
			<div id="tagline"><h3>Personalized. Affordable. Convenient.</h3>
<p><em>The ideal online Torah tutoring platform for children grades K-12</em>		<br />
			Mishna | Gemara | Chumash | Navi | Hebrew Language and More</p>
							
			</div>
            <form action="/./tutoring/?nts-panel=anon%2Fregister" method="POST">
             <input type="text" name="email" placeholder="your-email@mail.com">
             <input type="submit" value="Sign Up" class="links">
            </form></div>
              </div>
        <div class="one-half">
            <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel"  data-interval="3000">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					<li data-target="#carousel-example-generic" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="images/hero-n1.png" alt="First slide">
                                            </div>
                    <div class="item">
                        <img src="images/hero-n2.png" alt="Second slide">
                    </div>
					<div class="item">
                        <img src="images/hero-n3.png" alt="Third slide">
                    </div>
					<div class="item">
                        <img src="images/hero-n4.png" alt="Fourth slide">
                    </div>
                   </div>
                </div>

        </div>
   
 </div></div><!--hero slider-->  
	
<div class="content-sidebar-wrap">
	<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
	<a name="works"></a><div class="works">
    <div class="wrap">
        <h2 class="section_title centered-text"><i class="fa fa-cogs" aria-hidden="true"></i> How it works</h2>
		<div class="one-half first">		
			<video style="width:100%;" controls>
              <source src="videos/how_it_works_video.mp4" type="video/mp4">
              Your browser does not support the video tag.
            </video>		    
		</div>
		<div class="one-half">
        
        <p><em class="p_start">Students are paired with</em> highly qualified tutors and learning specialists in Israel. 
		These tutors focus on the child’s own learning style to help them improve their skills. 
		Sessions take place using cutting-edge web-conferencing technology to tutor students over the internet. 
		Our tutors and learning specialists are trained on how to take advantage of technology to enhance student engagement. 
		Communication lines between Naaleh Tutors and parents and schools always remain open for feedback and discussion. 
		Give your child all the help he or she needs without the worry.</p>
		<a href="/./contact.php" class="links">Learn more<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
        </div>
        </div><!--end wrap-->
	</div><!--end of works-->

<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>

    <div class="ease">
    <div class="wrap">
    	<div class="one-third first" id="auto">
        	<a href=""><div class="flip-container" ontouchstart="this.classList.toggle('hover');">
			<div class="flipper">
				<div class="front">
				</div>
				<div class="back">
				<h4><i class="fa fa-calendar" aria-hidden="true"></i> <br />Ease Of Use</h4>
				</div>
			</div>
			</div></a>
			<p><em class="p_start"><i class="fa fa-calendar" aria-hidden="true"></i> Ease Of Use</em> Our simple online tutoring platform is all that you need to purchase tutoring sessions, create & manage appointments and meet with your private teacher. </p>
		</div>
		<div class="one-third" id="at_home"><a href=""><div class="flip-container" ontouchstart="this.classList.toggle('hover');">
			<div class="flipper">
				<div class="front">
				</div>
				<div class="back">
				<h4><i class="fa fa-home" aria-hidden="true"></i> <br /> At home</h4>
				</div>
			</div>
			</div></a>
			<p><em class="p_start"> <i class="fa fa-home" aria-hidden="true"></i> At home</em> No need to go anywhere or to factor in travel time for a tutoring session. Just schedule a time that’s convenient for you and study from anywhere.  Access the platform from your PC or mobile device, at home or on the go!</p>
		</div>
		<div class="one-third" id="affordable"><a href=""><div class="flip-container" ontouchstart="this.classList.toggle('hover');">
			<div class="flipper">
				<div class="front">
				</div>
				<div class="back">
				<h4><i class="fa fa-usd" aria-hidden="true"></i> <br /> Affordable</h4>
				</div>
			</div>
			</div></a>
			<p><em class="p_start"><i class="fa fa-usd" aria-hidden="true"></i> Affordable</em> Give your child the support he needs without financial concern. The NTT system has lowered costs, making tutoring affordable regardless of your budget.</p>
		</div>
		<div class="clear-line"></div>
        </div><!--end wrap-->
    </div><!--end ease-->
    
	<div class="tutors">
    <div class="wrap">
 		<h2 class="section_title"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Our Tutors</h2>
    	<p><em class="p_start">Our tutors possess years of experience</em> teaching and studying Torah. Our learning specialists have excellent credentials and will help your child succeed!  </p>
        <a href="/./tutors.php" class="links">Learn more<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
    </div><!--end wrap-->
    </div><!--end tutors-->
</div>
</div>
<?php include('inc_files/footer.inc'); ?>
