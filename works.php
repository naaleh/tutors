
<!DOCTYPE html>
<html lang="en-GB">
<head itemscope itemtype="http://schema.org/WebSite">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naaleh Torah Tutors: How it Works</title>
<meta name="description" content="At Naaleh Torah Tutors, students are paired with highly qualified tutors using cutting-edge web-conferencing technology to tutor students over the internet. " />

<?php 
$class = "about";
include('inc_files/header.inc'); ?>
          
<div class="site-inner">

	<h1 id="page_header"><i class="fa fa-cogs" aria-hidden="true"></i> How it Works</h1>
		<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>	

		
<div id="about_list">
<div class="wrap">

<ul>
<li><div class="list_n">1</div> A student is registered as a new student on the Naaleh Tutors site.</li>
<li><div class="list_n">2</div>The parent or school is contacted by Naaleh Tutors to discuss the specifics of the student’s needs. </li>
<li><div class="list_n">3</div>Naaleh pairs the student with an appropriate tutor or learning specialist. </li>
<li><div class="list_n">4</div>The Student receives their own student page that will include payments, appointments, and other important information. </li>
<li><div class="list_n">5</div>Appointments are scheduled through an easy to use automated appointment system </li>
<li><div class="list_n">6</div>The Session is conducted via web-conferencing and recorded and saved onto the students page. </li>
<li><div class="list_n">7</div>Optional Built-in incentive program to encourage student progress.</li>
<li><div class="list_n">8</div>Feedback and evaluations of sessions are completed by all parties.</li> 
</ul>
</div>
		</div><!--end wrap-->
		</div><!--end inner content-->
		</div>
</div><!--end of inner-->

<?php include('inc_files/footer.inc'); ?>
