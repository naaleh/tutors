
<!DOCTYPE html>
<html lang="en-GB">
<head itemscope itemtype="http://schema.org/WebSite">
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Naaleh Torah Tutors: About Us</title>
<meta name="description" content="Naaleh Torah Tutors strives to give every Jewish student around the world the individual attention and academic support of one-on-one tutoring, at an affordable price." />

<?php 
$class = "about";
include('inc_files/header.inc'); ?>

          
<div class="site-inner">

	<h1 id="page_header"><i class="fa fa-users" aria-hidden="true"></i> About Us</h1>
		<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>	
<div class="page_intro">
    <div class="wrap">
        <p>Naaleh Tutors is subdivision of Beis Midrash of Queens, a non-profit charitable and educational 
			organization dedicated to strengthening Torah learning in the Jewish Community and the world, 
			through innovative programs.   Naaleh Tutors is an outgrowth of <a href="http://www.naaleh.com/">Naaleh.com</a>, 
			the free Torah educational website providing thousands of inspirational video lectures to close to 30,000 users across the globe.  
			The Naaleh Tutors website is sponsored through a grant from <a href="http://avichai.org/">AVI CHAI</a>.
		</p> </div><!--end wrap-->
	</div><!--end of page intro-->
	
<div class="content-sidebar-wrap">
	<div class="inner_content">
		<div class="wrap">
					<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
			<h2 class="section_title first"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Our Mission</h2>
			<p>  <em class="p_start">  Naaleh Torah Tutors strives to give </em>every Jewish student around the world the individual attention and academic support of one-on-one tutoring, at an affordable price. We aim to provide the international Jewish Community with an affordable option to support their children’s Jewish Education, allowing their kids to improve their Judaic Studies skills while developing a warm relationship with a dedicated role model. 
			</p>
			<h2 class="section_title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Our Method</h2>
			<p><em class="p_start"> Students are paired with</em> highly qualified private teachers or learning specialists. These tutors focus on the child’s own learning style to help them improve their skills. Sessions take place using cutting-edge web-conferencing technology to teach students using the internet. Our tutors and learning specialists are trained on how to take advantage of technology to enhance student engagement. Communication lines between Naaleh Tutors and parents and schools always remain open for feedback and discussion. Give your child all the help he or she needs without the worry.
			</p>
			</div>
			<div class="image-line wrap">
					<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div>
			<img src="images/about_images.jpg"  no-repeat center />
							<div class="color-bar">
		<div class="color-block"></div>
		<div class="color-block"></div>
		<div class="color-block" id="color-block-3"></div>
		<div class="color-block" id="color-block-2"></div>
		<div class="color-block" id="color-block-1"></div>
	</div></div>
<!--<div id="about_list">
<div class="wrap">
<h2 class="section_title"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> How it works</h2>
<ul>
<li><div class="list_n">1</div> A student is registered as a new student on the Naaleh Tutors site.</li>
<li><div class="list_n">2</div>The parent or school is contacted by Naaleh Tutors to discuss the specifics of the student’s needs. </li>
<li><div class="list_n">3</div>Naaleh pairs the student with an appropriate tutor or learning specialist. </li>
<li><div class="list_n">4</div>The Student receives their own student page that will include payments, appointments, and other important information. </li>
<li><div class="list_n">5</div>Appointments are scheduled through an easy to use automated appointment system </li>
<li><div class="list_n">6</div>The Session is conducted via web-conferencing and recorded and saved onto the students page. </li>
<li><div class="list_n">7</div>Optional Built-in incentive program to encourage student progress.</li>
<li><div class="list_n">8</div>Feedback and evaluations of sessions are completed by all parties.</li> 
</ul>
</div>
		</div>--><!--end wrap-->
		</div><!--end inner content-->
		</div>
</div><!--end of inner-->

<?php include('inc_files/footer.inc'); ?>
